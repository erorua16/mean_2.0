//CONFIGURATION
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const todoRoutes = require("./routes/Todo");
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use("/api", todoRoutes);

//DATABASE CONNECTION
mongoose
  .connect(
    "mongodb+srv://erorua16:simplon@cluster0.17vth.mongodb.net/todoapp?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log("connection to database succeeded");
  })
  .catch((error) => {
    console.error("Error" + error.message);
  });

// PORT CONFIGS
const port = process.env.PORT || 6000;
app.listen(port, () => {
  console.log("Server open on http://localhost:" + port);
});
